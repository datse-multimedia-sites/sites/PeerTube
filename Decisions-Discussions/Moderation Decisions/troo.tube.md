# Troo.tube

This one should probably just go to this:

Contact form used to send the following:

> Don't like the videos don't come around! Get lost! I won't remove any videos 
> from my site and you're wasting time reposting them, dont be such a shit head.

Here's the thing.  The admin decided that my abuse reports are so terrible that
they decided to contact me, through the admin contact.  Send an abusive email
about that.

Knows how to do that.  But refuses to handle anything about actually using 
the moderation tools to make moderation "work" for them, but rather will be
abusive to someone who uses the "report video" which *always* will report
to the originating instance, as well as the instance the person is reporting 
from.

There are videos which we feel fall quite firmly in the 
"dangerous misinformation" as well as the "hate speech" categories.

But rather than actually going through those details, if you wish to see 
anything about that, you can go to the instance yourself.

The email which was used, is not a real email, even though they have set 
their DNS up to indicate it is valid.  I found valid email, and apparently
they only wanted to complain about the fact that we used a tool, as it was
designed.

The latest message suggests that they thought the above message conveyed 
the idea that "the software is buggy, please understand this."  
