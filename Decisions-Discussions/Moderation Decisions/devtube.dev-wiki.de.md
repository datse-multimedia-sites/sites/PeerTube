# devtube.dev-wiki.de

This instance was muted because of the number of ads which were coming 
from the instance.  10 of the 24 latest videos are ads.  There is a 
banner which says: "Please donate to keep this service free of ads and
limits".

I suspect that they are getting nothing from the ads that are getting 
posted, but they also are not doing anything to remove them.  
