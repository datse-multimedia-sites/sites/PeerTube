# v.sevvie.ltd

This instance has a large portion of conspiracy theory videos, some
of which may be "harmless themself" and might even contain useful 
information, but the style of conspiracy theory videos leads to a weird
situation of greater ease in manipulating the ideas which a person 
believes.

There are only 20 videos, and it looks like they are *all* conspiracy
theory videos.  And they are all under one account.  
