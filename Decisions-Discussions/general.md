This is for decisions and discussion (though probably mostly myself) about 
policies, and moderation, or whatever.  This repository was setup because of
wanting to look at certain types of content we see, and how we want to deal with
that.  

Specifically we want to look at how to deal with content in videos, and what 
content will be allowed, and what content will not be allowed.

Also, (this seems harder) looking at comments which show up in response to
videos, and seeing what we want to do about that.