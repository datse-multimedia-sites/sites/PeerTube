Writing this directly on Gitlab.  A few things have happened with the instance that lead to us deciding to shut it down, and several other sites.

The decision came a great deal because of how our host treated us around a copyright claim.  Our questioning how the claim was validated was answered with, "it was a valid claim" which is a total non answer.

In fact this answer was given *multiple* times, but apparently pointing out that it wasn't a sufficient to get any other answer.

After having spent *hours* trying to get someone to actually answer with anything beyond that, it was clear that, "we support all our customers" either means that the person making the *claim* was a customer, and somehow supercedes us (and they won't say) or it's what in the venacular is called a lie.

That is, it's demonstraably false statement which can be confirmed to be false by the thread of conversation where they continued to insist that the reason we were getting screwed over, and not getting *any* real answers from them was because "we support all our customers".

Technically, they haven't lost us, but we are currently only paying about 10% of what we were, and are not sure we will open any other VPSes with them in the future.  We're really not OK with being lied to.

The desire to *not* have any more servers with them, has lead us to try to figure out what might work for other places to have our services hosted.

We can not have our services hosted on our own home network, because it keeps going down, and for PeerTube it simply would *not* work.  

We have looked to see if there is anything available in Canada, and we were unable to find anything which we were happy with.  Our DNS registration is in Canada, and they do offer hosting.  But we've really wanted to stay away from having hosting and DNS registration on the same company, as it has been clear when we've done that, it's been nothing but problems if any of that we want to move elsewhere.  

We, have been considering what we would want to know when we decide to start PeerTube up again (and we are wanting to do so):

- Find hosting
- Reduce instances we follow
- Consider our policy around copyright
- Consider our policy around allowing people to post videos
- Consider our policy around user registration
- Consider our policy around advertising

Those are probably important things to be clear on.

Copyright is probably the trickiest of these decisions, as the claim was geofenced, but we couldn't geofence reasonably.

Also, we suspect that the claim was based not on a violating instance, but a honeypot instance.  It wasn't content we had hosted on our own instance.  And apparently that doesn't matter, a company can setup a honeypot and use it to destroy as much as possible PeerTube.

Even though, PeerTube is not really meant to be the "copyright killer" that makes it make sense for them to *try* that.  There is plenty of material which gets hosted, which it's either not clear whether who is posting it has rights, the rights are based on creative commons licenseses which are too narrowly defined such as CC-BY-NC-ND-SA (order?) which is about as good as putting on your work, "you're free to use this for personal use if you don't do anything with it."  Ie. it's basically copyright anyway.  

Even with permissive licenses as our preffered, CC-BY-SA or simly CC-BY there are places that will use them in ways which violates the license.  Could I send a copyright notice and expect it to be respected to each of the ~30 places which are using (mostly in violation) one of my CC-BY-SA images (it's all one image)?

I don't *want* to.  I would be *very* happy if they actually used it correctly, rather than just stealing it, or only giving a bare (but insuficient) attribution, with no indication that they are sharing their derivative work alike...  Or even indicating that my work is under any CC license.  

It's not that I have a problem with how my work is getting used (though calling matcha, alfalfa powder is a bit annoying).  I honestly wouldn't have much problem if I had *intentionally* licensed it as copyright.

My biggest problem is that a national broadcaster (CBC) is using it, incorrectly, and I doubt I could get them to stop, or to actually correctly use it.

So, I welcome people who may have interest in getting this back up (including other members of our system) to give some feedback on this.