We are adopting a policy similar to the 
[github DMCA guide](https://docs.github.com/en/github/site-policy/guide-to-submitting-a-dmca-takedown-notice), 
most notably we *will* publish the the notice received, regardless of how it is received.

While we attempt to handle material which is hosted on other sites, due to how federation works, requesting 
the removal from *our* instance of material from elsewhere is simply removing links to content in most
cases.

We will have a directory with *all* received requests of this sort.  Our resources are limited so it
is important to make sure that you provide as *specific* information as you can provide.

In most cases, a simple request stating the URL on our site is likely insuficiently specific unless
it is something directly hosted on our site.

If it is content from another instance, we require a statement that you have taken all action possible
to address that site, prior to contacting us.  

