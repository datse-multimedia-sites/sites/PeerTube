This is just about what some of our thoughts about muting are.

As we have followed more instances we've found that there are more decisions
to make about what accounts, and possibly by extension what instances to mute.

There are certain types of content which we want to keep off of our instance,
as it seems that it's not "good faith".

## Account Muting

This section is mostly about muting accounts, or blacklisting videos.  

### Advertising

As stated I think on the instance about page, we do not accept advertising.

So if your video is advertising, it may be blacklisted because of that.

Also stated around the advetising, is that if a video is creative with the 
primary purpose as advertising or promotion, that is OK.  

Also, we probably would be OK if what is being done is informative about what
is being promoted.  Ie. there is real information in your video.

#### Infomercials

This will not include "infomercial" type content which proports to be 
informative but in actuallity is simply leading you along to a purchase. 

This is considered to be a dangerously manipulative form of advertising.  

### Harmful speech

We've also noticed that there is a variety of harmful speech, it seems that 
there are a few varieties which we've run across so far:

- Speech promoting discriminaton against other people (particularly minorities)
  - We'll call it "hate speech" here.
- Speech promoting ideas which are likely to cause harm, if taken seriouesly.
  - We'll call this "dangerous misinformation" here.
  
#### Hate speech

Any speech which promotes discrimation against other people.  If you speak of
"American Virtues Sink to New Lows" and use the term "fag flag" in your 
description, this is considered enough to blacklist your video.

We've not yet come up with other examples of this yet.

#### Dangerous Misinformation

Here it gets pretty tricky, though there have been a few topics which have come
up which qualify under this:

- Antivaccine content
- Anti personal protective equipment

Especially at this time (almost exactly 7 months after the first case of 
COVID-19) both of these are extremely dangerous.  

#### Religious adherence to a belief system

This is where a person makes statements to indicate that all criticism of their
view is misinformed, and that they have some infalable source for the 
information which means they can simply dismiss the views contrary to them.

This is composed of these elements:

- A belief system which cannot be challanged
- A automatic discredding of any criticism as invalid
- A automatic acceptance of any criticism of the critics as valid
 
The last may not apply to all who criticize the immutable truth.

### Other content

Currently there have been some more tricky things, around what we want to do
with certain NSFW type videos.

We don't really *want* to have porn on our site.  And I guess the main thing 
around what we consider to be stuff which we would actually be OK with deciding
that it's not an acceptable video, is mostly around the, "is this harmful" and
most of what we can say about that is if the content is "exploitation."  

Further input of course would be accepted.  

## Instance muting

This is more about when I will consider that there is a reason to mute an 
instance.  

### Single topic

An instance which has only one topic likely will end up getting muted, if that
is in one of the above categories.  

One instance we have done so with has anti-vaccine contnet.  

### Large numbers of muted accounts

If a large number of the accounts are muted, then we will mute the instance.

Though with using the report feature, we may see if the accounts end up getting
better handled on the remote instance before deciding to mute the entire 
instance.  So far we've mutted one instance because we'd muted at least 10 
accounts on that instance in very short order.  

The specific decisions we make will be put in a different file, and I think
in this folder.  That only applies to future decisions.  We have 14 muted 
accounts, but as we didn't have a proper procedure, it has been a bit difficult
to know why a decision was made with a given account.  