Our Code of Conduct (which may be updated) guides our moderation.

We do not accept advertising.

Content which is discriminatory is not welcome.

Misleading content is not welcome.

Dangerous content is not welcome.

Further discussion can be seen on our [Gitlab Project](https://gitlab.com/datse-multimedia-sites/sites/PeerTube/) (please contact if that link is confusing).  