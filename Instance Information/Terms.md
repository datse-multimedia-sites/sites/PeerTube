**TERMS:**

0. These terms are intended primarily as guidelines, and not rules, as such the application of these as rules can be overridden on the basis that the intention is different from what these say.
1. We do not intend to be a hierarchical organization and as such we do not have any rules to define that the hierarchy overrules the community consensus.
2. We do have people who are responsible for maintaining the community and they are expected to be listened to, but are expected to listen to the community as a whole not just the community leadership.
3. Be nice to each other.
