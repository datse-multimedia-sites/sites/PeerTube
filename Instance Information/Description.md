We are a really small instance, which is mostly for a single user.  We allow registrations, and default to limiting to *no* uploads...

We will turn on an upload limit when a person requests it.

Really this is just to allow people to join, without too much trouble, but not have to worry that someone might post hours of unwanted content before we even see it.  

There likely will be changes to this policy in the future.  