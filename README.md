# PeerTube

Policies can be found at [Policies](Policies).

The public discussion about the policies and the decisions we have made can be 
seen in [Discussion](Decisions-Discussions).

Moderation decisions can be seen at [Moderation Decisions](Decisions-Discussions/Moderation%20Decisions)

All of this is a work in progress.  Hopefully it all makes sense, and we are able
to maintain everything...