# Ideas about who to follow/block/not follow

This is about mainly some ideas about who we will end up following when (if?) we setup a new instance.

## Instances we follow

### First Follows:

These are instances that are quite important and are instances we either have an account on, or have considered an acount on:

* [share.tube](https://share.tube/)
* [diode.zone](https://diode.zone/)
* [kolektiva.media](https://kolektiva.media/)
* [peertube.linuxrocks.online](https://peertube.linuxrocks.online/)
* [vidcommons.org](https://vidcommons.org/)
* [blender.org](https://video.blender.org/)
* [privacy.io](https://tube.privacytools.io/)
* [XR Tube](https://tube.rebellion.global/)

Those three are ones that I can think of right off the top.

After those we look at what those instances follow, and maybe a few which have content that the administrators want to have access to.

### Second Follows

I will carefully look at the instances that my instance follows and add the instances they follow.  I am currently active on 
share.tube so can say that there are no instances that they follow (which are active) which are not OK to follow.

The other two I am less sure about, though diode.zone I know is quite good at moderation, so I would suspect their list is pretty decent.

There are some instances which we like a lot of the content which gets shared by them, but which seem to not have good moderation happening.

The list above probably will be added to with a few more instances, as "primary" follows.  

## Instances we allow to follow us

Generally, we have no problems with instances that follow us.  In the time we have run (at various times) PeerTube, there's only been
a handful of instances which we've been so not OK with, that we don't want them following us.

### Instances violating copyright overtly:

Instances where it is clear that a big part of what they are doing is providing the ability to violate copyright we 
will not allow to follow us.

We don't really believe in the copyright system as it currently works (though we do accept that it may have started out good), 
but because of some of the problems with how the copyright system works, when there is blatant abuse, it seems prudent to both
protect our content from being republished, and to reduce the risk of copyright claims against our instance to not even allow
them to follow back.

### Instances with high proportion of dangerous misinformation

There are some instances which seem to have been setup largely to host content which gets removed or whatever from the larger
platforms because it is considered by them to be dangerous misinformation.  

I don't feel that such sites are worth allowing to follow, as they have made it clear that they are uninterested in moderating content.

### Instances with high numbers of low quality ads

(grrr I should be doing this in vi or otherwise on my computer)

Some instances have ads such as, "X local dentist" or such that are clearly mass produced.  Our site will never be ad supported
and we won't support instances which apear to either be supporting ads (ie. they aren't getting anything from it, but the ads remain),
or are ad supported.

### Instances with cryptocurency monitization

Some instances have set things up to allow cryptocurency monitization, and this is similar to our instance.  This is a way which
increases the likelyhood that people will create content "for the algorithm" rather than actually creating content with the 
intention of it being high quality.

Other than those reasons, we really have no reason not to allow instances to follow.

### Following back instances that follow us.

Some of the instances which follow us are likely to be instances which we'd be interested in their content.

There are generally two ways that people decide who to follow, either they follow based on what they want to see, or they do bulk follows.

In either of these cases, it can be good content that is there.

### Member requests:

At the time we last ended up shutting down, we were seeing that there were options being introduced to allow direct following off of 
content which isn't formally followed by the instance.

I would need to know better how that is working, and whether that brings that content to the "recently added" similarly to 
when it is added as following an instance, or if it's something which only the person following can see.

In the case it shows up as a follow of an *account* on an instance, rather than a whole instance, I will probably in 
most cases leave that as is, unless the whole instance looks interesting.

If on the other hand, it's very low visibility, I may consider simply following the instance if it's one that we would be 
interested in following.

## Instance blocks

Generally we try to block accounts rather than instances.  Since when anyone who is moderating would be reporting and
reporting to the originating instance, it can be relatively clear that an instance is not handling moderation in a way
which we are comfortable with.  

In cases as above we may block instances at a quick basis when it looks like their content violates our policies.

