# Redundancy

We have enabled redundancy and currently have 75GB assigned to handling 
redundancy.  This appears to be working, though it's not fully handling
the issue which we keep running into of certain instances we are 
interested in, often having difficulty serving the videos.  

We are manually adding the instances which we allow redundancy on, mainly on
the basis of, they actually are of interest, and they can have problems.

Some are being added because we particualarly want to support the instance
by offering some of our bandwidth to them.  

This likely will change over time.  

## Instances with Redundancy Enabled

* tube.odat.xyz
* vid.wizards.zone
* peertube.parleur.net
* evertron.tv
* diode.zone
* vid.lubar.me
* vidcommons.org
* share.tube
* video.hardlimit.com
* pony.tube
* tilvids.com
* video.blender.org
* dialup.express
* peertube.linuxrocks.online
* peertube.dsmouse.net
* kolektiva.media
* peertube.floss-marketing-school.com


