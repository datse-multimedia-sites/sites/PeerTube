There are two previously setup projects with some information about them.

There is the Datse Multimedia Sites/General information which contains general
information about what policies apply to all of our sites, and also to what 
sites those are.  

There is the Datse Multimedia Sites/Sites/Social which is from when we were 
running a Mastodon instance, and some decisions and policies from that time.